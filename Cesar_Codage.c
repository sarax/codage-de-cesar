#include <stdio.h>
#include <stdlib.h>
int estMinuscule(char c){
    if(c>='a' && c<='z')
    return 1;
    else
        
        return 0;
}
int estMajuscule(char c){
    if(c>='A' && c<='Z')
    return 1;
    else
    
        return 0;
}
int main(int argc, char *argv[]) {
    

    
    char *fichier_entree = argv[1];
    int decalage = atoi(argv[2]);
    char *fichier_sortie = argv[3];

    // Ouvrir le fichier d'entrée
    FILE *entree = fopen(fichier_entree, "r");
    if (entree == NULL) {
        perror("Erreur lors de l'ouverture du fichier d'entrée");
        return 1;
    }

    // Ouvrir le fichier de sortie
    FILE *sortie = fopen(fichier_sortie, "w");
    if (sortie == NULL) {
        perror("Erreur lors de l'ouverture du fichier de sortie");
        fclose(entree);
        return 1;
    }

    
    /*int caractere;
    while ((caractere = fgetc(entree)) != EOF) {
        fputc(caractere, sortie);
    }*/
    if(decalage<0){
        decalage=decalage+((-decalage/26) +1)*26;
    }
    printf("%d\n",decalage);
    int caractere;
    while ((caractere = fgetc(entree)) != EOF) {
        if (estMinuscule(caractere) || estMajuscule(caractere)) {
            // Vérifier si le caractère est une lettre non accentuée
            char base = estMajuscule(caractere) ? 'A' : 'a';
            caractere = (caractere - base + decalage) % 26 + base;
             fputc(caractere, sortie);
        }else
        fputc(caractere, sortie);
    }

    
    fclose(entree);
    fclose(sortie);

    printf("Le fichier a été copié avec succès.\n");

    return 0;
}
