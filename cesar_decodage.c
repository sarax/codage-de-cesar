#include<stdio.h>
#include<stdlib.h>
int estMinuscule(char c){
    if(c>='a' && c<='z')
    return 1;
    else
        
        return 0;
}
int estMajuscule(char c){
    if(c>='A' && c<='Z')
    return 1;
    else
        
        return 0;
}


char prochaineLettre(FILE *in){
    char caractereNul='\0';
    if(in==NULL){
        perror("erreur lors de l'ouverture du fichier");
        return 1;
    }
    int caractere;
    caractere=fgetc(in);

    while(caractere!=EOF){
        if(estMinuscule(caractere) || estMajuscule(caractere)){
        
        printf("%c\n",caractere);
        return caractere;
        }
        else
            caractere=fgetc(in);
    }
    return caractereNul;
    
}
   
char lettreLaPlusFrequente(FILE *in) {
    char lettreFreq = '\0'; 
    int compteurMax = 0;
    
    if (in == NULL) {
        perror("Echec Ouverture");
        return '\0';
    }
   int compteur[26] = {0}; // Un tableau pour compter les occurrences de chaque lettre
    
    char c;
    while ((c = prochaineLettre(in)) != '\0') {

        int index = c - 'a';
        compteur[index]++;
        if (compteur[index] > compteurMax) {
            compteurMax = compteur[index];
            lettreFreq = c;
        }
    }
    
    if (lettreFreq != '\0') {
        printf("La lettre la plus fréquente est : %c\n", lettreFreq);
    } else {
        printf("Le fichier est vide ou ne contient aucune lettre.\n");
    }
    
    return lettreFreq;
}

void decodageCesar(FILE *in, FILE *out){
    char lettreFreq=lettreLaPlusFrequente(in);
    int decalage='e'-lettreFreq;
    fseek(in,0,SEEK_SET);

    if(decalage<0){
        decalage=decalage+((-decalage/26) +1)*26;
    }
    printf("%d\n",decalage);

  
    int caractere;
    while ((caractere = fgetc(in)) != EOF) {
        if (estMinuscule(caractere) || estMajuscule(caractere)) {
            // Vérifier si le caractère est une lettre non accentuée
            char base = estMajuscule(caractere) ? 'A' : 'a';
            caractere = (caractere - base + decalage) % 26 + base;
            fputc(caractere, out);
        }
        else
        fputc(caractere, out);
    }






  
}

int main(int argc, char *argv[]){
    
    char *fichier_entree = argv[1];
    char *fichier_sortie = argv[2];

    FILE *monfichier=fopen(fichier_entree,"rb");
    FILE *monfichierSortie=fopen(fichier_sortie,"wb");

    decodageCesar(monfichier,monfichierSortie);
    return 0;
}
